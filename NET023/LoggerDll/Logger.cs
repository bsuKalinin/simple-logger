﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Listener;

namespace LoggerDll
{
    public class Logger
    {
        private readonly List<IEntityListener> _logListeners = new List<IEntityListener>();
        private object _configObject;

        public LogLevel MinLevel { get; set; }

        public Logger()
        {
            BeginningLog();
        }

        public Logger(LogLevel min)//only for tests
        {
            MinLevel = min;
            _logListeners = new List<IEntityListener>();
        }

        public void Trace(string message)
        {
            WriteInfo(message, LogLevel.Trace);
        }

        public void Debug(string message)
        {
            WriteInfo(message, LogLevel.Debug);
        }

        public void Info(string message)
        {
            WriteInfo(message, LogLevel.Info);
        }

        public void Warn(string message)
        {
            WriteInfo(message, LogLevel.Warn);
        }

        public void Error(string message)
        {
            WriteInfo(message, LogLevel.Error);
        }

        public void Fatal(string message)
        {
            WriteInfo(message, LogLevel.Fatal);
        }

        public IEnumerable<Tuple<object, object>> Track(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            Console.WriteLine(obj.GetType().IsClass);
            IEnumerable<Tuple<object, object>> properties = null;
            IEnumerable<Tuple<object, object>> fields = null;
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static |
                                 BindingFlags.Instance;

            if (obj.GetType().GetCustomAttribute<TrackingEntityAttribute>() != null)
            {
                properties = obj.GetType().GetProperties(flags)
                    .Where(z => z.GetCustomAttribute<TrackingPropertyAttribute>() != null)
                    .Select(x => new Tuple<object, object>(x.Name, x.GetValue(obj)));
                fields = obj.GetType().GetFields(flags)
                    .Where(z => z.GetCustomAttribute<TrackingPropertyAttribute>() != null)
                    .Select(x => new Tuple<object, object>(x.Name, x.GetValue(obj)));
            }

            return properties?.Concat(fields);
        }

        private void WriteInfo(string message, LogLevel logLevel)
        {
            CheckArgs(message);

            if (logLevel < MinLevel)
            {
                throw new ArgumentOutOfRangeException(nameof(logLevel));
            }

            foreach (IEntityListener logListener in _logListeners)
            {
                logListener.WriteInfo(logLevel.ToString(), message);
            }
        }

        private void BeginningLog()
        {
            _configObject = ConfigurationManager.GetSection("logger");
            CheckArgs(_configObject);

            LoggerSection config = (LoggerSection)_configObject;
            MinLevel = (LogLevel)Enum.Parse(typeof(LogLevel), config.MinLogLevel);

            Listeners listeners = config.Listeners;
            foreach (Listener listener in listeners)
            {
                Type listenerType = Type.GetType(listener.Type);
                Type listenerValuesType = Type.GetType(listener.Values);
                if (listenerValuesType == null) continue;
                dynamic logListenerArgs = Activator.CreateInstance(listenerValuesType);

                PropertiesCollection listenerProperties = listener.PropertiesCollection;
                foreach (Property listenerProperty in listenerProperties)
                {
                    PropertyInfo propertyInfo = listenerValuesType.GetProperty(listenerProperty.Name);
                    if (propertyInfo != null)
                    {
                        propertyInfo.SetValue(logListenerArgs, listenerProperty.Value);
                    }
                }

                if (listenerType == null) continue;
                dynamic logListener = Activator.CreateInstance(listenerType, args: logListenerArgs);
                _logListeners.Add(logListener);
            }
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}