﻿using System;
using Listener;

namespace WordListener
{
    public class WordConfig : EntityListenerConfig
    {
        private string _path;
        private string _filename;
        private string _fontsize;
        private string _shadow;
        private string _fontname;

        public string Fontsize
        {
            get => _fontsize;
            set
            {
                CheckArgs(value);
                _fontsize = value;
            }
        }
        public string Shadow
        {
            get => _shadow;
            set
            {
                CheckArgs(value);
                _shadow = value;
            }
        }
        public string Fontname
        {
            get => _fontname;
            set
            {
                CheckArgs(value);
                _fontname = value;
            }
        }
        public string Path
        {
            get => _path;
            set
            {
                CheckArgs(value);
                _path = value;
            }
        }
        public string FileName
        {
            get => _filename;
            set
            {
                CheckArgs(value);
                _filename = value;
            }
        }

        public WordConfig() : this(" ", " ", " ", "0", "0") { }

        public WordConfig(string path, string filename, string fontname, string fontsize, string shadow)
        {
            Path = path;
            FileName = filename;
            Fontname = fontname;
            Fontsize = fontsize;
            Shadow = shadow;
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
        }
    }
}