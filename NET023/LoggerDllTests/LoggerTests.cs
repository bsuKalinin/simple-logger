﻿using System;
using System.Collections.Generic;
using System.Linq;
using Listener;
using LoggerDll;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET023;

namespace LoggerDllTests
{
    [TestClass]
    public class LoggerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Track_object_IsNull_Exception()
        {
            Logger logger = new Logger();
            logger.Track(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Trace_message_IsNull_Exception()
        {
            Logger logger = new Logger();
            logger.Trace(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Log_Level_Less_Min_Exception()
        {
            Logger logger = new Logger(LogLevel.Fatal);
            logger.Trace("str");
        }

        [TestMethod]
        public void Track_Class_IsTrue()
        {
            TestClass testclass = new TestClass(10) {Age = 10, IsStudent = false, Name = "student1"};

            Logger logger = new Logger(LogLevel.Debug);
            List<Tuple<object, object>> values = logger.Track(testclass).ToList();
            List<string> originList = new List<string> {"Age 10", "Name student1", "_group 10"};
            bool b = true;
            for (int i = 0; i < values.Count(); ++i)
            {
                string str = values[i].Item1 + " " + values[i].Item2;
                if (!string.Equals(str, originList[i]))
                {
                    b = false;
                    break;
                }
            }

            Assert.AreEqual(true, b);
        }


        [TestMethod]
        public void Track_Struct_IsTrue()
        {
            TestStruct testclass = new TestStruct(10) {Age = 10, IsStudent = false, Name = "student1"};

            Logger logger = new Logger(LogLevel.Debug);
            List<Tuple<object, object>> values = logger.Track(testclass).ToList();
            List<string> originList = new List<string> {"Age 10", "Name student1", "_group 10"};
            bool b = true;
            for (int i = 0; i < values.Count(); ++i)
            {
                string str = values[i].Item1 + " " + values[i].Item2;
                if (!string.Equals(str, originList[i]))
                {
                    b = false;
                    break;
                }
            }

            Assert.AreEqual(b, true);
        }

        
    }
}