﻿using LoggerDll;

namespace NET023
{
    [TrackingEntity]
    public class TestClass
    {
        [TrackingProperty("group")] private int _group;

        [TrackingProperty("Age")] public int Age { get; set; }
        [TrackingProperty("Name")] public string Name { get; set; }
        public bool IsStudent { get; set; }

        public TestClass(int group)
        {
            _group = group;
        }

    }
}